<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>DYFI KANNUR</title>
<link rel="icon" href="../../images/shortcut.png" />
<script type="text/javascript" src="../../js/modernizr-1.5.min.js"></script>
<script type="text/javascript" src="../../js/jquery-2.1.4.min.js"></script>

<!-- Bootstrap -->
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/style.css" rel="stylesheet">
<link href="../../css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="../../css/owl.carousel.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../../font-awesome/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="../../css/jQuery-pagination.css"/>
<link rel="stylesheet" type="text/css" href="../../css/bootstrap-theme.css"/>
<script src="../../js/jquery_pagination.js"></script>
<!--<script src="../../js/tinymce.min.js"></script>-->

<script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="bgmain">
<div id="header">
  <div class="container"> <a href="http://webadmin.dyfikannur.com/office/banner/new.php" class="logo"></a>
    <div class="navigation">
      <div class="dropdown"> <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> Settings <span class="caret"></span> </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
          <li><a href="../changePassword/new.php">Change Password</a></li>
          <li><a href="../../logout.php">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="clearer"></div>
</div>
<div id="contentarea">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-4 leftnav">
        <div class="leftmenu">
          <ul>
            <li> <a href="../banner/new.php" class="dropdown-toggle" > <span class="menuicons icn1"></span>Banner</a></li>			
            
            <li> <a href="../committee/new.php" class="dropdown-toggle"> <span class="menuicons icn2"></span>Committee</a>
				
			</li>
			
			<li> <a href="../gallery/new.php" class="dropdown-toggle"> <span class="menuicons icn2"></span>Gallerry</a>
				
			</li>
			<li> <a href="../events/new.php" class="dropdown-toggle"> <span class="menuicons icn2"></span>News and Events</a>
				
			</li>
			</ul>
        </div>
      </div>