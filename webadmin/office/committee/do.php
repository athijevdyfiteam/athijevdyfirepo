<?php
session_start();
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
//header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['name'])
			{
				$_SESSION['msg']="<font color='red'>Error, Invalid Details!</font>";				
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$data['name']	  		=	$App->convert($_REQUEST['name']);						
				$data['designation']	  		=	$App->convert($_REQUEST['designation']);	
				$data['contact']	  		=	$App->convert($_REQUEST['contact']);						
					
				$db->query_insert(TABLE_COMMITTEE,$data);							
				$db->close();				
				$_SESSION['msg']="<font color='green'>Committee Member Added Successfully</font>";					
				header("location:new.php");						
			}
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  		    
		if(!$_POST['name'])
			{	
				$_SESSION['msg']="<font color='red'>Error, Invalid Details.</font>";									
				header("location:edit.php?id=$editId");	
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['name']	 =	$App->convert($_REQUEST['name']);
				$data['designation']	 =	$App->convert($_REQUEST['designation']);
				$data['contact']	 =	$App->convert($_REQUEST['contact']);
					
				$db->query_update(TABLE_COMMITTEE,$data," ID='{$editId}'");								
				$db->close();
				
				$_SESSION['msg']="<font color='green'>Member Details Updated successfully</font>";					
				header("location:new.php");					
			}		
		
		break;		
	// DELETE SECTION
		case 'delete':
			$id = $_REQUEST['id'];
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);	
			$db->connect();	
			
			$db->query("DELETE FROM `".TABLE_COMMITTEE."` WHERE ID='{$id}'");								
			$db->close();
			$_SESSION['msg']="<font color='green'>Product Deleted Successfully</font>";					
			header("location:new.php");	
						
		break;
}
?>


















