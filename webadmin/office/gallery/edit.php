<?php include("../adminHeader.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<?php
 if(isset($_SESSION['msg'])){?><?php echo $_SESSION['msg']; ?><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];
	$tableEdit=mysql_query("SELECT * FROM `".TABLE_GALLERY."` WHERE ID='$editId'");	
	$editRow=mysql_fetch_array($tableEdit);
?>

 
      <!-- Modal1 -->
      <div>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">GALLERY DETAILS</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">
                <div class="row">
                 	<div class="col-sm-12">
                 		<div class="cur_image">
                 			<img src="../../<?= $editRow['galleryPath']; ?>"/>
                 		</div>               
	                    <div class="form-group">
	                      <label for="countType">Change Image: * (900x400) </label>
	                      <input id="image1" name="galleryImage" type="file" class="file-loading" accept="image/*">
	                    </div>  
	                    <div class="form-group">
	                      <label for="countType">Title: </label>
                      	  <input type="text" class="form-control2" name="title" id="name" value="<?= $editRow['title']; ?>" >
	                    </div> 
	                    <div class="form-group">
	                      <label for="countType">Description: </label>
                      	  <textarea name="description" class="form-control2"><?= $editRow['description']; ?></textarea>
	                    </div> 
					</div> 					                                     
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
  
  <script>
  tinymce.init({
    selector: '#tinyText'
  });
  </script>
<?php include("../adminFooter.php") ?>
