<?php include("../adminHeader.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<?php
 if(isset($_SESSION['msg'])){?><?php echo $_SESSION['msg']; ?><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];
	$tableEdit=mysql_query("SELECT * FROM `".TABLE_EVENTS."` WHERE ID='$editId'");	
	$editRow=mysql_fetch_array($tableEdit);
?>

 
      <!-- Modal1 -->
      <div>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">EVENT DETAILS</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">
                <div class="row">
                 	<div class="col-sm-12">
	                    <div class="form-group">
	                      <label for="countType">Event: </label>
                      	  <input type="text" required="required" class="form-control2" name="name" id="name" value="<?= $editRow['event']; ?>" >
	                    </div> 
	                    <div class="form-group">
	                      <label for="countType">Description: </label>                      	  
                      	  <textarea name="description" required="" class="form-control2" style="min-height: 100px;"><?= $editRow['description']; ?></textarea>
                      	  
	                    </div> 
	                    <div class="form-group">
	                      <label for="countType">Current Date: </label>
                      	  <p><?= $App->dbformat_date_db($editRow['eventDate']); ?></p>
	                    </div> 
	                    <div class="form-group">
	                      <label for="countType">Change Date: </label>
                      	  <input type="text" required="required" class="form-control2 datepicker" name="date" id="name" value="<?= $App->dbformat_date_db($editRow['eventDate']); ?>" >
	                    </div>
					</div> 					                                     
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
  
  <script>
  tinymce.init({
    selector: '#tinyText'
  });
  </script>
<?php include("../adminFooter.php") ?>
