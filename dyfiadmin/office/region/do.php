<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
session_start();

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		if(!$_REQUEST['regionName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
								
				$regionName			=	$App->convert($_REQUEST['regionName']);
				$regionName			=	ucwords(strtolower($regionName));	
						
				$existId			=	$db->existValuesId(TABLE_REGION," region='$regionName'");
				
				if($existId>0)
				{
					$_SESSION['msg']="Region Is Already Exist";	
					header("location:new.php");												
				}
				else
				{
					$data['region']			=	$App->convert($regionName);
					
					$success	=	$db->query_insert(TABLE_REGION,$data);	
					
					//inserting to tracking table
					$data1['tableName']		=	'TABLE_REGION';	
					$data1['status']		=	'ADD';	
					$data1['rowID']			=	$success;	
					$data1['date']			=	date("Y-m-d");	
					$success1	=	$db->query_insert(TABLE_TRACKING,$data1);
												
					if($success)
					{							
						$_SESSION['msg']="Region Added Successfully";										
					}
					else
					{
						$_SESSION['msg']="Failed";
					}
					$db->close();
					header("location:new.php");
				}
			}		
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id']; 
			    	
			if(!$_REQUEST['regionName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
								
				$regionName		=	$App->convert($_REQUEST['regionName']);
				$regionName		=	ucwords(strtolower($regionName));			
				$existId		=	$db->existValuesId(TABLE_REGION,"region='$regionName' and ID!=$editId");
				if($existId>0)
				{
					$_SESSION['msg']="Region Is Already Exist";													
				}
				else
				{
					$data['region']			=	$App->convert($regionName);
					
					$success=$db->query_update(TABLE_REGION,$data," ID='{$editId}'");
					
					//inserting to tracking table
					$data1['tableName']		=	'TABLE_REGION';	
					$data1['status']		=	'EDIT';	
					$data1['rowID']			=	$editId;	
					$data1['date']			=	date("Y-m-d");	
					$success1	=	$db->query_insert(TABLE_TRACKING,$data1);
					if($success)
					{							
						$_SESSION['msg']="Region Updated Successfully";										
					}
					else
					{
						$_SESSION['msg']="Failed";
					}
					$db->close();
				}
				
				header("location:new.php");		
											
			}	
			break;	
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{
					$success= @mysql_query("DELETE FROM `".TABLE_REGION."` WHERE ID='{$deleteId}'");				      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}		
				
				//inserting to tracking table
				$data1['tableName']		=	'TABLE_REGION';	
				$data1['status']		=	'DELETE';	
				$data1['rowID']			=	$deleteId;	
				$data1['date']			=	date("Y-m-d");	
				$success1	=	$db->query_insert(TABLE_TRACKING,$data1);
													
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Region details deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:new.php");					
		break;		
}
?>