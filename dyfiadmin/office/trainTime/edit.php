<?php include("../adminHeader.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];
	$tableEdit=mysql_query("select TT.ID,
								 TT.fromPlace,
								 TT.toPlace,
								 TT.trainName,
								 TT.trainNum,
								 TT.days,
								 TT.via
						   from `".TABLE_TRAIN."` TT 
					       WHERE TT.ID='$editId'
					       order by TT.ID desc
					       ");	
	$editRow=mysql_fetch_array($tableEdit);
	if($editRow['days'])
	{
		$daysArray = explode(",",$editRow['days']);
		//print_r($daysArray);die;
	}
	
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">TRAIN DETAILS </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">
				<div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="fromPlace">From :*</label>
                      <input type="text" name="fromPlace" id="fromPlace" class="form-control2" required value="<?php echo $editRow['fromPlace'];?>" >                     
                    </div>
                    <div class="form-group">
                      <label for="toPlace">To :*</label>
                      <input type="text" name="toPlace" id="toPlace" class="form-control2" required value="<?php echo $editRow['toPlace'];?>" >                    
                    </div>
                    <div class="form-group">
                      <label for="countType">Train Name: </label>
                     <input type="text" class="form-control2" name="trainName" id="trainName" value="<?php echo $editRow['trainName'];?>" >
                    </div> 
                    <div class="form-group">
                      <label for="countType">Train Number: </label>                    
                      <input type="text" class="form-control2" name="trainNum" id="trainNum" value="<?php echo $editRow['trainNum'];?>" >
                    </div>                   
					<div class="form-group">
                      <label for="via">Via: </label>
                      <input type="text" name="via" id="via" class="form-control2" value="<?php echo $editRow['via'];?>" >	
                    </div>
				  </div>
                 
                   <div class="col-sm-6">
                    <div class="form-group">
                      <label for="date">Days:</label>
                       <table class="table">
						<tr>										
							<td>Sun</td>
							<td><input type="checkbox" name="chk_group[]" value="Sun" <?php if(@in_array('Sunday',$daysArray)){ echo "checked";} ?>></td>		
						</tr>
						<tr>										
							<td>Mon</td>
							<td><input type="checkbox" name="chk_group[]"  value="Mon" <?php if(@in_array('Mon',$daysArray)){ echo "checked";} ?>></td>		
						</tr>
						<tr>										
							<td>Tue</td>
							<td><input type="checkbox" name="chk_group[]" value="Tues" <?php if(@in_array('Tues',$daysArray)){ echo "checked";} ?>></td>		
						</tr>
						<tr>										
							<td>Wed</td>
							<td><input type="checkbox" name="chk_group[]" value="Wednes" <?php if(@in_array('Wednes',$daysArray)){ echo "checked";} ?>></td>		
						</tr>
						<tr>										
							<td>Thu</td>
							<td><input type="checkbox" name="chk_group[]" value="Thurs" <?php if(@in_array('Thurs',$daysArray)){ echo "checked";} ?>></td>		
						</tr>
						<tr>										
							<td>Fri</td>
							<td><input type="checkbox" name="chk_group[]" value="Fri" <?php if(@in_array('Fri',$daysArray)){ echo "checked";} ?>></td>		
						</tr>
						<tr>										
							<td>Sat</td>
							<td><input type="checkbox" name="chk_group[]" value="Satur" <?php if(@in_array('Satur',$daysArray)){ echo "checked";} ?>></td>		
						</tr>							
					  </table>
                    </div>                  					
                   </div>                  
                </div>
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
