<?php
session_start();
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['fromPlace'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
							
				$data['fromPlace']	=	$App->convert($_REQUEST['fromPlace']);
				$data['toPlace']	=	$App->convert($_REQUEST['toPlace']);
				$data['trainName']	=	$App->convert($_REQUEST['trainName']);
				$data['trainNum']	=	$App->convert($_REQUEST['trainNum']);
				$data['via']		=	$App->convert($_REQUEST['via']);
				if(isset($_POST['chk_group']))					
				{
					$daysArray = $_POST['chk_group'];
					$newDays   = implode(",", $daysArray);
				}
				else
				{
					$newDays	=	'';
				}
				
				$data['days']	=	$newDays;
				
				$db->query_insert(TABLE_TRAIN,$data);								
				$db->close();
				
				$_SESSION['msg']="Train Details Added Successfully";					
				header("location:new.php");						
			}
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  		    
		if(!$_POST['fromPlace'])
			{	
				$_SESSION['msg']="Error, Invalid Details.";									
				header("location:edit.php?id=$editId");	
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['fromPlace']	=	$App->convert($_REQUEST['fromPlace']);
				$data['toPlace']	=	$App->convert($_REQUEST['toPlace']);
				$data['trainName']	=	$App->convert($_REQUEST['trainName']);
				$data['trainNum']	=	$App->convert($_REQUEST['trainNum']);
				$data['via']		=	$App->convert($_REQUEST['via']);	
				
				if(isset($_POST['chk_group']))					
				{
					$daysArray = $_POST['chk_group'];
					$newDays   = implode(",", $daysArray);
				}
				else
				{
					$newDays	=	'';
				}
				
				$data['days']	=	$newDays;
				
				$db->query_update(TABLE_TRAIN,$data," ID='{$editId}'");								
				$db->close();
				
				$_SESSION['msg']="Train Details Updated successfully";					
				header("location:new.php");					
			}		
		
		break;		
	// DELETE SECTION
	case 'delete':		
				$id	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				$db->query("DELETE FROM `".TABLE_TRAIN."` WHERE ID='{$id}'");	
				$db->query("DELETE FROM `".TABLE_TRAIN_TIME."` WHERE trainId='{$id}'");							
				$db->close(); 
				$_SESSION['msg']="Train Details Deleted Successfully";					
				header("location:new.php");					
		break;	
		
		// insert time
	case 'time':		
				if(!$_REQUEST['trainId'])
				{
					$_SESSION['msg']="Error, Invalid Details!";					
					header("location:new.php");		
				}
			else
				{				
					$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
					$db->connect();
					$trainId		=	$App->convert($_REQUEST['trainId']);
						
						$success1	=	0;
						
						$place     =  ucfirst(strtolower( $App->convert($_POST['place'])));
						$time      =  strtoupper($App->convert($_POST['time']));
						$existId   =  $db->existValuesId(TABLE_TRAIN_TIME,"place = '$place' and trainId='$trainId'");
						
						if($existId==0)
						{
							$data['place']	 	=	$place;
							$data['time']		=	$time;
						    $data['trainId']	=	$trainId;						
						    $success1			=	$db->query_insert(TABLE_TRAIN_TIME,$data);
						   // die;
						}
						
						$db->close();
						if($success1)
						{							
							$_SESSION['msg']="Time Added Successfully";										
						}
						else{
							$_SESSION['msg']="Time already registered or Failed to register";
						}
						header("location:new.php");			
				}									
		break;
		
		// DELETE REGION
	case 'delTime':		
				$deleteId	=	$_REQUEST['deleteId'];
				$trainId	=	$_REQUEST['sid'];
				$success1	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{ 
					$success1	= @mysql_query("DELETE FROM `".TABLE_TRAIN_TIME."` WHERE ID='{$deleteId}'");
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete this";				            
				}											
				$db->close(); 
				if($success1)
				{
					$_SESSION['msg']="Train time deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete this";										
				}	
				header("location:new.php");						
		break;	
}
?>