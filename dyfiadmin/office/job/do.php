<?php
session_start();
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['typeID'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
							
				$data['typeID']	=	$App->convert($_REQUEST['typeID']);
				$data['name']	 		 =	$App->convert($_REQUEST['name']);
				$data['phone']	  		=	$App->convert($_REQUEST['phone']);							
					
				$db->query_insert(TABLE_JOB,$data);								
				$db->close();
				
				$_SESSION['msg']="Job Details Added Successfully";					
				header("location:new.php");						
			}
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  		    
		if(!$_POST['typeID'])
			{	
				$_SESSION['msg']="Error, Invalid Details.";									
				header("location:edit.php?id=$editId");	
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['typeID']	 =	$App->convert($_REQUEST['typeID']);
				$data['name']	 		  =	$App->convert($_REQUEST['name']);
				$data['phone']	  		 =	$App->convert($_REQUEST['phone']);
					
				$db->query_update(TABLE_JOB,$data," ID='{$editId}'");								
				$db->close();
				
				$_SESSION['msg']="Job Details Updated successfully";					
				header("location:new.php");					
			}		
		
		break;		
	// DELETE SECTION
	case 'delete':		
				$id	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				$db->query("DELETE FROM `".TABLE_JOB."` WHERE ID='{$id}'");								
				$db->close(); 
				$_SESSION['msg']="Job Details Deleted Successfully";					
				header("location:new.php");					
		break;		
}
?>