<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['fname'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
		
				$data['teaterName']		=	$App->convert(ucwords(strtolower($_REQUEST['tname'])));					
				$data['filmName']		=	$App->convert(ucwords(strtolower($_REQUEST['fname'])));					
				$data['description']	=	$App->convert($_REQUEST['description']);					
				$data['noOfShows']		=	$App->convert($_REQUEST['noshows']);					
				$data['filmDate']		=	$App->dbformat_date($_REQUEST['date']);					
					
				$db->query_insert(TABLE_FILM,$data);								
				$db->close();
				
				$_SESSION['msg']="Film Added Successfully";					
				header("location:new.php");						
			}
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  		    
		if(!$_POST['fname'])
			{	
				$_SESSION['msg']="Error, Invalid Details.";									
				header("location:edit.php?id=$editId");	
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['teaterName']		=	$App->convert(ucwords(strtolower($_REQUEST['tname'])));					
				$data['filmName']		=	$App->convert(ucwords(strtolower($_REQUEST['fname'])));					
				$data['description']	=	$App->convert($_REQUEST['description']);					
				$data['noOfShows']		=	$App->convert($_REQUEST['noshows']);					
				$data['filmDate']		=	$App->dbformat_date($_REQUEST['date']);					
					
				$db->query_update(TABLE_FILM,$data," ID='{$editId}'");
												
										
				$db->close();
				
				$_SESSION['msg']="Film Updated successfully";					
				header("location:new.php");					
			}		
		
		break;		
	// DELETE SECTION
	case 'delete':		
				$id	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				$db->query("DELETE FROM `".TABLE_FILM."` WHERE ID='{$id}'");								
				$db->close(); 
				$_SESSION['msg']="Film Deleted Successfully";					
				header("location:new.php");					
		break;	
		// NEW REGION
	case 'time':
		
		if(!$_REQUEST['filmId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$filmId		=	$App->convert($_REQUEST['filmId']);
					
				    $success1	=	0;
													 
					  
						   $data['filmId']		=	$filmId;
						   $data['filmTime']	=	$App->convert($_REQUEST['time']);					
						   $success1			=	$db->query_insert(TABLE_FILM_TIME,$data);
					
					$db->close();
							if($success1)
							{							
								$_SESSION['msg']="Time Details Added Successfully";										
							}
							else{
								$_SESSION['msg']="failed";
							}
					header("location:new.php");		
			}											
					
		break;	
	// DELETE REGION
	case 'delTime':		
				$deleteId	=	$_REQUEST['deleteId'];
				//$serviceId	=	$_REQUEST['sid'];
				$success1	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
					
				try
				{ 
					$success1	= @mysql_query("DELETE FROM `".TABLE_FILM_TIME."` WHERE ID='{$deleteId}'");	
					//echo "DELETE FROM `".TABLE_FILM_TIME."` WHERE ID='{$deleteId}'";
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete.";				            
				}											
				$db->close(); 
				if($success1)
				{
					$_SESSION['msg']="time deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. ";										
				}	
				header("location:new.php");						
		break;	
}
?>