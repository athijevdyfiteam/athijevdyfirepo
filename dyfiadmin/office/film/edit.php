<?php include("../adminHeader.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];
	$tableEdit=mysql_query("SELECT * FROM `".TABLE_FILM."` WHERE ID='$editId'");	
	$editRow=mysql_fetch_array($tableEdit);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog" style="margin:0px auto">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">FILM </h4>
            </div>
            <div class="modal-body clearfix">
				<div class="row">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">
                
                  
                  <div class="col-sm-6">     
            
                   
                    <div class="form-group">
                      <label for="tname">Teater Name:*</label>
                        <input type="text" class="form-control2" name="tname" id="tname" value="<?php echo $editRow['teaterName']; ?>" required>	
                    </div>
                     <div class="form-group">
                      <label for="fname">Film Name:*</label>
                        <input type="text" class="form-control2" name="fname" id="fname" value="<?php echo $editRow['filmName']; ?>" required>	
                    </div>
                    <div class="form-group">
                      <label for="noshows">Number of Shows:*</label>
                        <input type="text" class="form-control2" name="noshows" id="noshows" value="<?php echo $editRow['noOfShows']; ?>" required>	
                    </div>
                   	<div class="form-group">
                      <label for="description">Description:</label>
                        <textarea  class="form-control2" name="description" style="margin: 0px; width: 265px; height: 57px;"  id="description" ><?php echo $editRow['description']; ?>	</textarea>
                    </div>
                    <div class="form-group">
                      <label for="date">Date:*</label>
                        <input type="text" class="form-control2 datepicker" name="date" id="date" value="<?php echo $App->dbformat_date_db($editRow['filmDate']); ?>" required>	
                    </div>			
                   </div> 
                 
               
                                              
					<div class="modal-footer" style="float:left; width:100%">
					  <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
					</div>			
			 	
				
			</form>
			</div>
          </div>
        </div>
      </div>
	 </div>
      <!-- Modal1 cls --> 
     
   
<?php include("../adminFooter.php") ?>
