<?php
session_start();
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['heading'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
							
				$data['heading']	=	$App->convert($_REQUEST['heading']);
				$data['description']  	=	$_REQUEST['description'];
				$data['contactNo']	=	$App->convert($_REQUEST['phone']);
				$data['postDate']	=	$App->convert($_REQUEST['cDate']);							
					
				$lastID=$db->query_insert(TABLE_MARKET,$data);								
				$db->close();
				
				if(isset($_REQUEST['push']))
				{
					$APPLICATION_ID = "O1REpMkGFOuwJsTNF2GFyNnobyYtmHtTiGFUd8N5";
					$REST_API_KEY = "OdFHcGPspBCMxRwHWfq7aAqWCE2iZRsPzxFhh8rm";
					$MESSAGE = $App->convert($_REQUEST['heading']);
					
					$act="com.iakremera.pushnotificationdemo.UPDATE_STATUS";
									
					$url = 'https://api.parse.com/1/push';
					$marketData="{'id':'".$lastID."','heading':'".$MESSAGE."' ,'description':'".$_REQUEST['description']."','phno':'".$_REQUEST['phone']."','postDate':'".$_REQUEST['cDate']."'}";
					 	
				        $data = array(
				            'channel' => 'newshirt',
				            'type' => 'android',
				            'expiry' => 1451606400,
				            'data' => array(
				                'alert'  => $MESSAGE,
				                'action' => $act,
				                'title'  => 'DYFI APP',
				                'marketData' =>$marketData,
				            ),
				        );
					$_data = json_encode($data);
					$headers = array(
						'X-Parse-Application-Id: ' . $APPLICATION_ID,
						'X-Parse-REST-API-Key: ' . $REST_API_KEY,
						'Content-Type: application/json',
						'Content-Length: ' . strlen($_data),
					);
			
					$curl = curl_init($url);
					curl_setopt($curl, CURLOPT_POST, 1);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($curl);
				}
				
				$_SESSION['msg']="Market Details Added Successfully";					
				header("location:new.php");						
			}
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  		    
		if(!$_POST['heading'])
			{	
				$_SESSION['msg']="Error, Invalid Details.";									
				header("location:edit.php?id=$editId");	
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['heading']	  =	$App->convert($_REQUEST['heading']);
				$data['description']  =	$_REQUEST['description'];
				$data['contactNo']	=	$App->convert($_REQUEST['phone']);
				$data['postDate']	 =	$App->convert($_REQUEST['cDate']);	
					
				$db->query_update(TABLE_MARKET,$data," ID='{$editId}'");								
				$db->close();
				
				$_SESSION['msg']="Market Details Updated successfully";					
				header("location:new.php");					
			}		
		
		break;		
	// DELETE SECTION
	case 'delete':		
				$id	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				$db->query("DELETE FROM `".TABLE_MARKET."` WHERE ID='{$id}'");								
				$db->close(); 
				$_SESSION['msg']="Market Details Deleted Successfully";					
				header("location:new.php");					
		break;		
}
?>