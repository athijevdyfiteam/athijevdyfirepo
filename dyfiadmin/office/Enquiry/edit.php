<?php include("../adminHeader.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];
	$tableEdit=mysql_query("SELECT * FROM `".TABLE_ENQUIRY."` WHERE ID='$editId'");	
	$editRow=mysql_fetch_array($tableEdit);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">ENQUIRY DETAILS </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="courseName">Date:*</label>
                      <input type="text" name="eDate" id="eDate" class="form-control2 datepicker" value="<?php echo $editRow['eDate'] ?>" required>
                    </div>
                    <div class="form-group">
                      <label for="place">Student Name:*</label>
                        <input type="text" class="form-control2" name="studentName" id="studentName" value="<?php echo $editRow['studentName'] ?>" required>	
                    </div>
                    <div class="form-group">
                      <label for="countType">Class: </label>
                      <input type="text" class="form-control2" name="class" id="class"  value="<?php echo $editRow['class'] ?>">
                    </div>
                    <div class="form-group">
                      <label for="subjectId">Subject: </label>
                      <select name="subjectId" id="subjectId" class="form-control2" required >
							<option value="">Select</option>
							<?php 
									$select2="select * from ".TABLE_COURSE."";
									$res2=mysql_query($select2);
									while($row2=mysql_fetch_array($res2))
									{
										if($row2['feeType']!='Package')							
										{
									?>
									<option value="<?php echo $row2['ID']?>" <?php if($editRow['subject']== $row2['ID']){?> selected="selected"<?php }?>><?php echo $row2['courseName']."-".$row2['countType']."-".$row2['place']."-".$row2['feeType']?></option>
									<?php 
										}
										else
										{
										?>
									<option value="<?php echo $row2['ID']?>"<?php if($editRow['subject']== $row2['ID']){?> selected="selected"<?php }?>><?php echo $row2['courseName']."-".$row2['countType']."-".$row2['place']."-".$row2['feeType']."-".$row2['duration']?></option>
									<?php 
										}
									}
							?>				
						</select>
                    </div>
					<div class="form-group">
                      <label for="countType">Phone: </label>
                      <input type="text" name="mobile" id="mobile" class="form-control2" value="<?php echo $editRow['phone'] ?>" >	
                    </div>
					</div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="fee">School:</label>
                      <input type="text" name="school" id="school" class="form-control2" value="<?php echo $editRow['school'] ?>">
                    </div>
					
                    <div class="form-group">
                      <label for="regFee">Location:</label>
                      <input type="text" name="location" id="location" class="form-control2" value="<?php echo $editRow['landmark'] ?>">
                    </div>							 
                  
                    <div class="form-group">
                      <label for="duration">Fee Details:</label>
                      <input type="text" class="form-control2" name="fee" id="fee" value="<?php echo $editRow['fee'] ?>">					 
                    </div>
									
                    <div class="form-groupy">
                      <label for="durationP">Remark: </label>
                      <textarea id="remark" name="remark" class="form-control2"><?php echo $editRow['remark'] ?></textarea>			  
                    </div>					
                   </div> 
                 
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
