<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['studentName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
			
					$data['eDate']			=	$App->convert($_REQUEST['eDate']);
					$data['studentName']	=	$App->convert($_REQUEST['studentName']);
					$data['class']			=	$App->convert($_REQUEST['class']);
					$data['subject']		=	$App->convert($_REQUEST['subjectId']);
					$data['phone']			=	$App->convert($_REQUEST['mobile']);
					$data['school']			=	$App->convert($_REQUEST['school']);
					$data['landmark']		=	$App->convert($_REQUEST['location']);
					$data['fee']			=	$App->convert($_REQUEST['fee']);
					$data['remark']			=	$App->convert($_REQUEST['remark']);
					
						
					$db->query_insert(TABLE_ENQUIRY,$data);								
					$db->close();
					
					$_SESSION['msg']="Enquiry Details Added Successfully";					
					header("location:new.php");						
			}
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  		    
		if(!$_POST['studentName'])
			{	
				$_SESSION['msg']="Error, Invalid Details.";									
				header("location:edit.php?id=$editId");	
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['eDate']			=	$App->convert($_REQUEST['eDate']);
				$data['studentName']	=	$App->convert($_REQUEST['studentName']);
				$data['class']			=	$App->convert($_REQUEST['class']);
				$data['subject']		=	$App->convert($_REQUEST['subjectId']);
				$data['phone']			=	$App->convert($_REQUEST['mobile']);
				$data['school']			=	$App->convert($_REQUEST['school']);
				$data['landmark']		=	$App->convert($_REQUEST['location']);
				$data['fee']			=	$App->convert($_REQUEST['fee']);
				$data['remark']			=	$App->convert($_REQUEST['remark']);	
							
					
				$db->query_update(TABLE_ENQUIRY,$data," ID='{$editId}'");								
				$db->close();
				
				$_SESSION['msg']="Enquiry Details Updated successfully";					
				header("location:new.php");					
			}		
		
		break;		
	// DELETE SECTION
	case 'delete':		
				$id	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				$db->query("DELETE FROM `".TABLE_ENQUIRY."` WHERE ID='{$id}'");								
				$db->close(); 
				$_SESSION['msg']="Enquiry Details Deleted Successfully";					
				header("location:new.php");					
		break;		
}
?>