<?php

require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

session_start();

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['typeID'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
							
				$data['vehicleTypeId']	=	$App->convert($_REQUEST['typeID']);
				$data['regNo']			=	$App->convert($_REQUEST['regNo']);
				$data['name']	 		=	$App->convert($_REQUEST['name']);
				$data['phone']	  		=	$App->convert($_REQUEST['phone']);
				$data['location']	  	=	$App->convert($_REQUEST['location']);
																							
				$db->query_insert(TABLE_TAXI,$data);
												
				$db->close();
				
				$_SESSION['msg']="Taxi Details Added Successfully";					
				header("location:new.php");						
			}
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  		    
		if(!$_POST['typeID'])
			{	
				$_SESSION['msg']="Error, Invalid Details.";									
				header("location:edit.php?id=$editId");	
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['vehicleTypeId']	 =	$App->convert($_REQUEST['typeID']);
				$data['regNo']			 =	$App->convert($_REQUEST['regNo']);
				$data['name']	 		 =	$App->convert($_REQUEST['name']);
				$data['phone']	  		 =	$App->convert($_REQUEST['phone']);
				$data['location']	  	 =	$App->convert($_REQUEST['location']);
					
				$db->query_update(TABLE_TAXI,$data," ID='{$editId}'");								
				$db->close();
				
				$_SESSION['msg']="Taxi Details Updated successfully";					
				header("location:new.php");					
			}		
		
		break;		
	// DELETE SECTION
	case 'delete':		
				$id	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				$db->query("DELETE FROM `".TABLE_TAXI."` WHERE ID='{$id}'");								
				$db->close(); 
				$_SESSION['msg']="Taxi Details Deleted Successfully";					
				header("location:new.php");					
		break;		
}
?>