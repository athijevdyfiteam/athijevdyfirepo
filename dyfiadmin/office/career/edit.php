<?php include("../adminHeader.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];
	$tableEdit=mysql_query("SELECT * FROM `".TABLE_CAREER."` WHERE ID='$editId'");	
	$editRow=mysql_fetch_array($tableEdit);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">CAREER DETAILS</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">
                <div class="row">
                 <div class="col-sm-6">
                    <div class="form-group">
                      <label for="courseName">Type :*</label>
                      <select name="types" id="types" class="form-control2" required >
							<option value="Employee" <?php if($editRow['type']== "Employee"){?> selected="selected"<?php }?>>Employee</option>
                            <option value="Employer" <?php if($editRow['type']== "Employer"){?> selected="selected"<?php }?>>Employer</option>                           										
						</select>                         
                    </div> 
                    <div class="form-group">
                      <label for="heading">Heading: </label>
                      <input type="text" class="form-control2" name="heading" id="heading" value="<?php echo $editRow['heading'] ?>">
                    </div>                   
                    <div class="form-group">
                      <label for="countType">Contact No: </label>
                      <input type="text" class="form-control2" name="contact" id="contact" value="<?php echo $editRow['contactNo'] ?>">
                    </div>                   
					<div class="form-group">
                      <label for="countType">Message: </label>
                      <textarea name="message" id="message" class="form-control2" ><?php echo $editRow['message'] ?></textarea>
                    </div>
                    <div class="form-group">
                      <label for="countType">Date: </label>
                      <input type="text" name="cDate" id="cDate" class="form-control2 datepicker" value="<?php echo $editRow['postDate'] ?>">	
                    </div>
					</div> 					                                     
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
