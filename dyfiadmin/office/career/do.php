<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['types'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
							
				$data['type']		 	=	$App->convert($_REQUEST['types']);
				$data['heading']	  	=	$_REQUEST['heading'];
				$data['contactNo']		=	$App->convert($_REQUEST['contact']);
				$data['message']	  	=	$_REQUEST['message'];							
				$data['postDate']	 	=	$App->convert($_REQUEST['cDate']);											
					
				$db->query_insert(TABLE_CAREER,$data);								
				$db->close();
				
				$_SESSION['msg']="Career Details Added Successfully";					
				header("location:new.php");						
			}
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  		    
		if(!$_POST['types'])
			{	
				$_SESSION['msg']="Error, Invalid Details.";									
				header("location:edit.php?id=$editId");	
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['type']		 	=	$App->convert($_REQUEST['types']);
				$data['heading']	  	=	$_REQUEST['heading'];
				$data['contactNo']		=	$App->convert($_REQUEST['contact']);
				$data['message']	  	=	$_REQUEST['message'];							
				$data['postDate']	 	=	$App->convert($_REQUEST['cDate']);
					
				$db->query_update(TABLE_CAREER,$data," ID='{$editId}'");								
				$db->close();
				
				$_SESSION['msg']="Career Details Updated successfully";					
				header("location:new.php");					
			}		
		
		break;		
	// DELETE SECTION
	case 'delete':		
				$id	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				$db->query("DELETE FROM `".TABLE_CAREER."` WHERE ID='{$id}'");								
				$db->close(); 
				$_SESSION['msg']="Career Details Deleted Successfully";					
				header("location:new.php");					
		break;		
}
?>