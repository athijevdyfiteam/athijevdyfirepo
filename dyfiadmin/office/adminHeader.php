<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>DYFI</title>
<script type="text/javascript" src="../../js/modernizr-1.5.min.js"></script>
<script type="text/javascript" src="../../js/jquery.js"></script>

<!-- Bootstrap -->
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/style.css" rel="stylesheet">
<link href="../../css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<link href="../../css/owl.carousel.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../../font-awesome/css/font-awesome.min.css"/>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="bgmain">
<div id="header">
  <div class="container"> <a href="#" class="logo"></a>
    <div class="navigation">
      <div class="dropdown"> <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> Settings <span class="caret"></span> </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
          <li><a href="../changePassword/new.php">Change Password</a></li>
          <li><a href="../../logout.php">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="clearer"></div>
</div>
<div id="contentarea">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-4 leftnav">
        <div class="leftmenu">
          <ul>
            <li> <a href="../adminDash/admin-dash.php" class="dropdown-toggle" > <span class="menuicons icn1"></span> Home </a></li>
			
            <li> <a href="../notification/new.php" class="dropdown-toggle"> <span class="menuicons icn8"></span> General news</a></li>
            
            <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn2"></span> Education</a>
				<ul class="dropdown-menu dropdown2" role="menu">
                	<li><a href="../institute/new.php">Institutions</a></li>
					<li><a href="../course/new.php">Course Details</a></li>               
              	</ul>
			</li>
			
            <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn3"></span> Bus Time</a>		
              	<ul class="dropdown-menu dropdown2" role="menu">
                	<li><a href="../busPlace/new.php">Bus Stops</a></li>
					<li><a href="../busTime/new.php">Bus Time</a></li>               
              	</ul>
			 </li>
			
			<li><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn4"></span> Auto Taxi</a>
            	<ul class="dropdown-menu dropdown2" role="menu">
                	<li><a href="../autoTaxiType/new.php">Types</a></li>
					<li><a href="../autoTaxi/new.php">Auto Taxi</a></li>               
              	</ul>
            </li>
            
            <li> <a href="../region/new.php" class="dropdown-toggle"> <span class="menuicons icn2"></span>Region</a></li>
            
            <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn5"></span> Shop</a>
			  	<ul class="dropdown-menu dropdown2" role="menu">
                	<li><a href="../shopType/new.php">Shop Types</a></li>
					<li><a href="../shop/new.php">Shops</a></li>               
              	</ul>
			</li>
			
            <li> <a href="../bloodBank/new.php" class="dropdown-toggle"> <span class="menuicons icn6"></span> Blood Bank</a></li>
			
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn7"></span> Job</a>
			  	<ul class="dropdown-menu dropdown2" role="menu">
                	<li><a href="../jobType/new.php">Job Types</a></li>
					<li><a href="../job/new.php">Jobs</a></li>               
              	</ul>
			</li>
			
			<li> <a href="../career/new.php" class="dropdown-toggle"> <span class="menuicons icn8"></span> Career</a></li>
			
			<li> <a href="../Emergency/new.php" class="dropdown-toggle" > <span class="menuicons icn10"></span> Emergency</a></li>
			
			<li> <a href="../market/new.php" class="dropdown-toggle"> <span class="menuicons icn9"></span> Market</a></li>						
			
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn10"></span> Health</a>
			  	<ul class="dropdown-menu dropdown2" role="menu">
                	<li><a href="../healthType/new.php">Types</a></li>
					<li><a href="../health/new.php">Details</a></li>               
					<li><a href="../healthMsg/new.php">More</a></li>               
              	</ul>
			</li>
			
			<li><a href="../Resource/new.php" class="dropdown-toggle"> <span class="menuicons icn11"></span> Resource Person</a></li>
			<li> <a href="../ad/new.php" class="dropdown-toggle" > <span class="menuicons icn11"></span> Ad</a></li>
			<li> <a href="../feedback/new.php" class="dropdown-toggle" > <span class="menuicons icn8"></span> Feedback</a></li>
			<li> <a href="../film/new.php" class="dropdown-toggle"> <span class="menuicons icn2"></span> Film</a></li>
                        <li> <a href="../trainTime/new.php" class="dropdown-toggle"> <span class="menuicons icn3"></span> Train</a></li>
			</ul>
        </div>
      </div>