<?php include("../adminHeader.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
$rowsPerPage = ROWS_PER_PAGE;
?>
<script>
function delete_type()
{
	var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
		window.submit();
	}
	else
	{
		return false;
	}
}



</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
 ?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">Ad</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
          <div class="col-sm-4" >
            <form method="post">
              <div class="input-group">
                <input type="text" class="form-control"  name="name" placeholder="AD" value="<?php echo @$_REQUEST['name'] ?>">
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="type"></button>
                </span> </div>
            </form>
          </div>
        </div>
		 <?php	
			$cond="1";
			if(@$_REQUEST['name'])
			{			
				$cond=$cond." and ".TABLE_AD.".ad like'%".$_POST['name']."%'";
			}
			
			?>
			<div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table" >
                <thead>
                  <tr>
                    <th>SL No</th>
					<th>Ad</th>
                    <th>End Date</th>
                    <th>Pic1</th>
                    <th>Pic2</th>																
                  </tr>
                </thead>
                <tbody>
						<?php 
						$selAllQuery = "select * from `".TABLE_AD."` where $cond order by ID desc";
						$select1=mysql_query($selAllQuery);
						
						$number=mysql_num_rows($select1);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="10">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
							/*********************** for pagination ******************************/
							
							if(isset($_GET['page']))
							{
								$pageNum = $_GET['page'];
							}
							else
							{
								$pageNum =1;
							}
							$offset = ($pageNum - 1) * $rowsPerPage;						
							$select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
							$i=$offset+1;
							//use '$select1' for fetching
							/*************************** for pagination **************************/
							while($row=mysql_fetch_array($select1))
							{	
							$tableId=$row['ID'];
							?>
					  <tr>
						<td><?php echo $i; $i++; ?>
						  <div class="adno-dtls"> <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>
						  </div></td>
						
						<td><?php echo $row['ad']; ?></td>
                        <td><?php echo $App->dbformat_date_db($row['endDate']); ?></td>
                        <td> <img src="<?php echo $row['pic1']; ?>" width="60px" height="10px" alt=""/></td>
                        <td> <img src="<?php echo $row['pic2']; ?>" width="64px" height="98px" alt=""/></td>															
					  </tr>
					  <?php }
					  }
					  ?>                  
                </tbody>
              </table>
            </div>
            <!--*****************************************************************-->          
            	 <?php 
                  if($number>$rowsPerPage)
					{
					?>	
					 <br />	
					  <div class="pagerSC" align="center">
					<?php					
					$query   =  $db->query($selAllQuery);
					$numrows = mysql_num_rows($query);				
					$maxPage = ceil($numrows/$rowsPerPage);
					$self = $_SERVER['PHP_SELF'];
					$nav  = '';
					if ($pageNum - 5 < 1) {
					$pagemin = 1;
					} else {
					$pagemin = $pageNum - 5;
					};
					if ($pageNum + 5 > $maxPage) {
					$pagemax = $maxPage;
					} else {
					$pagemax = $pageNum + 5;
					};
					
					for($page = $pagemin; $page <= $pagemax; $page++)
					{
					   if ($page == $pageNum)
					   {
						  $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
					   }
					   else
					   {
					   		 if(@$search)
					   		 {
							 	$nav .= " <a href=\"$self?page=$page&rid=$search\">$page</a> ";
							 }
							 else
							 {
							 	$nav .= " <a href=\"$self?page=$page\">$page</a> ";
							 }
						  
					   }
					}
					?>
					 <?php
					if ($pageNum > 1)
					{
					   $page  = $pageNum - 1;
					   if(@$search)
					   {
						   $prev  = " <a href=\"$self?page=$page&rid=$search\">Prev</a> ";
						   $first = " <a href=\"$self?page=1&rid=$search\">First Page</a> ";
					   }
					   else
					   {
						   $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
						   $first = " <a href=\"$self?page=1\">First Page</a> ";
					   }
					}
					else
					{
					   $prev  = '&nbsp;';
					   $first = '&nbsp;';
					}
					
					if ($pageNum < $maxPage)
					{
					   $page = $pageNum + 1;
					   if(@$search)
					   {
						   	 $next = " <a href=\"$self?page=$page&rid=$search\">Next</a> ";
						     $last = " <a href=\"$self?page=$maxPage&rid=$search\">Last Page</a> ";
					   }
					   else
					   {
						   	 $next = " <a href=\"$self?page=$page\">Next</a> ";
						   	 $last = " <a href=\"$self?page=$maxPage\">Last Page</a> ";
					   }
					  
					}
					else
					{
					   $next = '&nbsp;';
					   $last = '&nbsp;';
					}
					echo $first . $prev . $nav . $next . $last;
					?>
					<div style="clear: left;"></div>
					</div>	 
				<?php
				}
                ?>
            
           <!-- ******************************************************************-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Ad</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-6">     
            
                    <div class="form-group">
                      <label for="name">Ad Title:*</label>
                        <input type="text" class="form-control2" name="ad" id="ad" required>	
                    </div>
                    <div class="form-group">
                      <label for="name">End Date:*</label>
                        <input type="text" class="form-control2 datepicker" name="end" id="end" required>	
                    </div>
                    <div class="form-group">
                      <label for="name">Picture 1:* (300*50)</label>
                      <input id="image1" name="image1" type="file" multiple class="file-loading" accept="image/*" required> 

                    </div>
                    <div class="form-group">
                      <label for="name">Picture 2:* (320*480)</label>
                       <input id="image2" name="image2" type="file" multiple class="file-loading" accept="image/*" required> 
                    </div>
                   				
                   </div> 
                 
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
