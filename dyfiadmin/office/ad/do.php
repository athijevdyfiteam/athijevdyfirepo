<?php
session_start();
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
//header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
				


		if(!$_REQUEST['ad'])
			{
				$_SESSION['msg']="Error, Invalid Details!";	
			//	die;				
				header("location:new.php");		
			}
		else
			{		
			//	ini_set(max_execution_time,1000);
			//	ini_set(max_input_time,1000);
			//	ini_set(memory_limit,'1024M');		
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
		
//=======================================		
		//		$title=trim(convert($_POST['ad']));

				$nimage1=$_FILES['image1']['tmp_name'];
				$nimage2=$_FILES['image2']['tmp_name'];
				$result = date("YmdHis");//$date->format('YmdHis');	
				$path_info =pathinfo($_FILES["image1"]["name"]);
				$path_info2 =pathinfo($_FILES["image2"]["name"]);
				
				$upload_file=$result.'_0.'.$path_info['extension'];				
				$upload_file2=$result.'_1.'.$path_info2['extension'];
				
				copy($nimage1,"uploads/$upload_file");
				$size = getimagesize("uploads/$upload_file");
				$w = $size[0];
				$h = $size[1];
				$normalwidth=$w/300;
				$normalthum=$w/50;
				$orginalwidth=$w/$normalwidth;
				$orginalhegiht=$h/$normalwidth;
				$test="$nimage1";
				$test=substr($test,'-3');
				if($w>300)
				{
				  if($test=='gif')
				   {
					 cropImage($orginalwidth, $orginalhegiht, "uploads/$upload_file", 'gif', "uploads/$upload_file");
				   }
				  else
				   {
					cropImage($orginalwidth, $orginalhegiht, "uploads/$upload_file", 'jpg', "uploads/$upload_file");
				   }
				}				
				else
				{
					copy($nimage1,"uploads/$upload_file");
				}
				
				
				copy($nimage2,"uploads/$upload_file2");
				$size = getimagesize("uploads/$upload_file2");
				$w = $size[0];
				$h = $size[1];
				$normalwidth=$w/320;
				$normalthum=$w/480;
				$orginalwidth=$w/$normalwidth;
				$orginalhegiht=$h/$normalwidth;
				$test="$nimage";
				$test=substr($test,'-3');
				if($w>320)
				{
				  if($test==gif)
				   {
					 cropImage($orginalwidth, $orginalhegiht, "uploads/$upload_file2", 'gif', "uploads/$upload_file2");
				   }
				  else
				   {
					cropImage($orginalwidth, $orginalhegiht, "uploads/$upload_file2", 'jpg', "uploads/$upload_file2");
				   }
				}
				
				else
				{
					copy($nimage2,"uploads/$upload_file2");
				}  
		
		
//=======================================			
			
				$data['ad']	  =	$App->convert($_REQUEST['ad']);
				$data['endDate'] =	$App->dbFormat_date($_REQUEST['end']);																	
				$data['pic1']	=	$App->convert("uploads/".$upload_file);
				$data['pic2']	=	$App->convert("uploads/".$upload_file2);
					
				$db->query_insert(TABLE_AD,$data);								
				$db->close();
				
				$_SESSION['msg']="Ad Details Added Successfully";					
				header("location:new.php");						
			}
				
		break;		
	
	// DELETE SECTION
	case 'delete':		
				$id	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();	
							
				$sql2 = "SELECT pic1,pic2 FROM `".TABLE_AD."` WHERE ID='$id'";
				$vou_record = 	$db->query_first($sql2);
				$pic1 = 	$vou_record['pic1'];
				$pic2 = 	$vou_record['pic2'];
				
				unlink($pic1) ;
				unlink($pic2) ;
								
				$db->query("DELETE FROM `".TABLE_AD."` WHERE ID='{$id}'");								
				$db->close(); 
				$_SESSION['msg']="Ad Details Deleted Successfully";					
				header("location:new.php");					
		break;		
}

function cropImage($w,$h,$filename,$stype,$dest)
{
	$filename = $filename;
	$width = $w;
	$height = $h;
	switch($stype) 
	{
			case 'gif':
			$simg = imagecreatefromgif($filename);
			break;
			case 'jpg':
			$simg = imagecreatefromjpeg($filename);
			break;
			case 'png':
			$simg = imagecreatefrompng($filename);
			break;
			
	}
	header('Content-type: image/jpeg');
	list($width_orig, $height_orig) = getimagesize($filename);
	$ratio_orig = $width_orig/$height_orig;
	if ($width/$height > $ratio_orig) 
	{
	   $width = $height*$ratio_orig;
	} 
	else 
	{
	   $height = $width/$ratio_orig;
	}	
	$image_p = imagecreatetruecolor($width, $height);	
	imagecopyresampled($image_p, $simg, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
	imagejpeg($image_p,$dest, 100);
}

?>