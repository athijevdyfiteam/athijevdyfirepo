<?php
session_start();
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['name'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
							
				$data['name']	  =	$App->convert($_REQUEST['name']);
				$data['type']	  =	$App->convert($_REQUEST['type']);
				$data['position']	 =	$App->convert($_REQUEST['position']);
				$data['contactNo']	 =	$App->convert($_REQUEST['phone']);							
					
				$db->query_insert(TABLE_RESOURCE,$data);								
				$db->close();
				
				$_SESSION['msg']="Resource Person Details Added Successfully";					
				header("location:new.php");						
			}
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  		    
		if(!$_POST['name'])
			{	
				$_SESSION['msg']="Error, Invalid Details.";									
				header("location:edit.php?id=$editId");	
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['name']	  =	$App->convert($_REQUEST['name']);
				$data['position']	 =	$App->convert($_REQUEST['position']);
				$data['contactNo']	 =	$App->convert($_REQUEST['phone']);
					
				$db->query_update(TABLE_RESOURCE,$data," ID='{$editId}'");								
				$db->close();
				
				$_SESSION['msg']="Resource Person Details Updated successfully";					
				header("location:new.php");					
			}		
		
		break;		
	// DELETE SECTION
	case 'delete':		
				$id	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				$db->query("DELETE FROM `".TABLE_RESOURCE."` WHERE ID='{$id}'");								
				$db->close(); 
				$_SESSION['msg']="Resource Person Details Deleted Successfully";					
				header("location:new.php");					
		break;		
}
?>