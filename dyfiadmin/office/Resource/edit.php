<?php include("../adminHeader.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];
	$tableEdit=mysql_query("SELECT * FROM `".TABLE_RESOURCE."` WHERE ID='$editId'");	
	$editRow=mysql_fetch_array($tableEdit);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">RESOURCE PERSONS DETAILS</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">
                <div class="row">
                 <div class="col-sm-6">
                    <div class="form-group">
                      <label for="courseName">Name :*</label>
                      <input type="text" class="form-control2" name="name" id="name" value="<?php echo $editRow['name'] ?>" required >
                    </div>  
                     <div class="form-group">
                      <label for="courseName">Type :*</label>
                      <select name="type" id="type" class="form-control2" required>
						  <option value="0" <?php if($editRow['type']== "0"){?> selected="selected"<?php }?>>Person</option>
						  <option value="1" <?php if($editRow['type']== "1"){?> selected="selected"<?php }?>>Office</option>
						</select>
                    </div>                    
                    <div class="form-group">
                      <label for="countType">Place:* </label>
                      <input type="text" class="form-control2" name="position" id="position" value="<?php echo $editRow['position'] ?>" required>
                    </div>                   
					<div class="form-group">
                      <label for="countType">Phone: </label>
                      <input type="text" name="phone" id="phone" class="form-control2" value="<?php echo $editRow['contactNo'] ?>" >	
                    </div>
					</div> 					                                     
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
