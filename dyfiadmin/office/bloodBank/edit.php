<?php include("../adminHeader.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];
	$tableEdit=mysql_query("SELECT * FROM `".TABLE_BLOOD."` WHERE ID='$editId'");	
	$editRow=mysql_fetch_array($tableEdit);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">BLOOD DETAILS</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">
                <div class="row">
                 <div class="col-sm-6">
                    <div class="form-group">
                      <label for="courseName">Group :*</label>
                      <select name="group" id="group" class="form-control2" required >							
							<option value="A+" <?php if($editRow['bloodGroup']== "A+"){?> selected="selected"<?php }?>>A+</option>
                            <option value="A-" <?php if($editRow['bloodGroup']== "A-"){?> selected="selected"<?php }?>>A-</option>
                            <option value="AB+" <?php if($editRow['bloodGroup']== "AB+"){?> selected="selected"<?php }?>>AB+</option>
                            <option value="AB-" <?php if($editRow['bloodGroup']== "AB-"){?> selected="selected"<?php }?>>AB-</option>
                            <option value="B+" <?php if($editRow['bloodGroup']== "B+"){?> selected="selected"<?php }?>>B+</option>										
                            <option value="B-" <?php if($editRow['bloodGroup']== "B-"){?> selected="selected"<?php }?>>B-</option>										
                            <option value="O+" <?php if($editRow['bloodGroup']== "O+"){?> selected="selected"<?php }?>>O+</option>										
                            <option value="O-" <?php if($editRow['bloodGroup']== "O-"){?> selected="selected"<?php }?>>O-</option>										
						</select>
                    </div>                   
                    <div class="form-group">
                      <label for="countType">Name: </label>
                      <input type="text" class="form-control2" name="name" id="name" value="<?php echo $editRow['name'] ?>">
                    </div>                   
					<div class="form-group">
                      <label for="countType">Phone: </label>
                      <input type="text" name="phone" id="phone" class="form-control2" value="<?php echo $editRow['phone'] ?>">	
                    </div>
                    
                     <div class="form-group">
                      <label for="countType">Region: </label>
                      	<select name="region" class="form-control2">
                      		<option value="">Select</option>
                      		<?php 
                      		$regionQry = mysql_query("SELECT * FROM ".TABLE_REGION."");
                      		while($regionRow = mysql_fetch_array($regionQry))
                      		{
							
                      		 ?>
                      		<option value="<?php echo $regionRow['ID'];?>" <?php if($editRow['regionId']==$regionRow['ID']){ echo "selected"; }?>><?php echo $regionRow['region'];?></option>
                      		<?php	
							}
							?>
                      	</select>
                    </div>
					</div> 					                                     
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
