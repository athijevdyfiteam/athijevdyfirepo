<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['fromId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
							
				$data['fromPlaceID']	=	$App->convert($_REQUEST['fromId']);
				$data['toPlaceID']	  =	$App->convert($_REQUEST['toId']);
				$data['fromTime']	   =	$App->convert($_REQUEST['fromTime']);
				$data['toTime']		 =	$App->convert($_REQUEST['toTime']);
				$data['busName']		=	$App->convert($_REQUEST['busName']);
				$data['via']			=	$App->convert($_REQUEST['via']);					
					
				$db->query_insert(TABLE_BUSTIME,$data);								
				$db->close();
				
				$_SESSION['msg']="Bus Time Details Added Successfully";					
				header("location:new.php");						
			}
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  		    
		if(!$_POST['fromId'])
			{	
				$_SESSION['msg']="Error, Invalid Details.";									
				header("location:edit.php?id=$editId");	
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['fromPlaceID']	=	$App->convert($_REQUEST['fromId']);
				$data['toPlaceID']	  =	$App->convert($_REQUEST['toId']);
				$data['fromTime']	   =	$App->convert($_REQUEST['fromTime']);
				$data['toTime']		 =	$App->convert($_REQUEST['toTime']);
				$data['busName']		=	$App->convert($_REQUEST['busName']);
				$data['via']			=	$App->convert($_REQUEST['via']);	
					
				$db->query_update(TABLE_BUSTIME,$data," ID='{$editId}'");								
				$db->close();
				
				$_SESSION['msg']="Bus Time Details Updated successfully";					
				header("location:new.php");					
			}		
		
		break;		
	// DELETE SECTION
	case 'delete':		
				$id	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				$db->query("DELETE FROM `".TABLE_BUSTIME."` WHERE ID='{$id}'");								
				$db->close(); 
				$_SESSION['msg']="Bus Time Details Deleted Successfully";					
				header("location:new.php");					
		break;		
}
?>