<?php
require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

// array for JSON response
	$json_response1=array();	
	$select1=mysql_query("select * from `".TABLE_BLOOD."`");		
	$number=mysql_num_rows($select1);
	if($number>0)
	{		
		while($row = mysql_fetch_assoc($select1))
		{
			$rows1['id']=$row['ID'];
			$rows1['bloodGroup']=$row['bloodGroup'];
			$rows1['name']=$row['name'];
			$rows1['phno']=$row['phone'];
			array_push($json_response1,$rows1);			
		}
	}
	
	$json_response2=array();
	$select1=mysql_query("select * from `".TABLE_PLACE."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows2['id']=$row['ID'];
			$rows2['place']=$row['place'];			
			array_push($json_response2,$rows2);			
		}										
	}
	
	$json_response3=array();
	$select1=mysql_query("select * from `".TABLE_BUSTIME."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows3['id']=$row['ID'];
			$rows3['fromPlace']=$row['fromPlaceID'];	
			$rows3['toPlace']=$row['toPlaceID'];			
			$rows3['fromTime']=$row['fromTime'];			
			$rows3['toTime']=$row['toTime'];			
			$rows3['busName']=$row['busName'];			
			$rows3['via']=$row['via'];			
			array_push($json_response3,$rows3);			
		}										
	}
	
	$json_response4=array();
	$select1=mysql_query("select * from `".TABLE_CAREER."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows4['id']=$row['ID'];
			$rows4['type']=$row['type'];
			$rows4['heading']=$row['heading'];
			$rows4['message']=$row['message'];
			$rows4['phno']=$row['contactNo'];
			$rows4['postDate']=$row['postDate'];			
			array_push($json_response4,$rows4);
		}										
	}

	$json_response5=array();
	$select1=mysql_query("select * from `".TABLE_EMERGENCY."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows5['id']=$row['ID'];
			$rows5['name']=$row['name'];
			$rows5['place']=$row['place'];
			$rows5['phno']=$row['phone'];			
			array_push($json_response5,$rows5);			
		}										
	}
	
	$json_response6=array();
	$select1=mysql_query("select * from `".TABLE_HEALTY_TYPE."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows6['id']=$row['ID'];
			$rows6['type']=$row['type'];			
			array_push($json_response6,$rows6);			
		}										
	}
	
	$json_response7=array();
	$select1=mysql_query("select * from `".TABLE_HEALTH."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows7['id']=$row['ID'];
			$rows7['typeId']=$row['typeID'];
			$rows7['name']=$row['name'];
			$rows7['place']=$row['place'];
			$rows7['phno']=$row['phone'];			
			array_push($json_response7,$rows7);			
		}										
	}
	
	$json_response8=array();
	$select1=mysql_query("select * from `".TABLE_HEALTH_MSG."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows8['id']=$row['ID'];
			$rows8['healthId']=$row['healthID'];	
			$rows8['message']=$row['message'];			
			array_push($json_response8,$rows8);			
		}										
	}		
	
	$json_response9=array();
	$select1=mysql_query("select * from `".TABLE_INSTITUTE."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows9['id']=$row['ID'];
			$rows9['type']=$row['type'];	
			$rows9['name']=$row['name'];	
			$rows9['address']=$row['address'];	
			$rows9['phno']=$row['phone'];			
			array_push($json_response9,$rows9);			
		}										
	}
	
	$json_response10=array();
	$select1=mysql_query("select * from `".TABLE_INST_DETAILS."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows10['id']=$row['ID'];
			$rows10['intitutionId']=$row['instituteID'];	
			$rows10['branch']=$row['branch'];	
			$rows10['totalSeat']=$row['totalSeat'];			
			array_push($json_response10,$rows10);			
		}										
	}
	
	$json_response11=array();
	$select1=mysql_query("select * from `".TABLE_JOB_TYPE."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows11['id']=$row['ID'];
			$rows11['jobType']=$row['jobType'];			
			array_push($json_response11,$rows11);			
		}										
	}
	
	$json_response12=array();
	$select1=mysql_query("select * from `".TABLE_JOB."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows12['id']=$row['ID'];
			$rows12['typeId']=$row['typeID'];
			$rows12['name']=$row['name'];
			$rows12['phno']=$row['phone'];			
			array_push($json_response12,$rows12);			
		}										
	}
	
	$json_response13=array();
	$select1=mysql_query("select * from `".TABLE_MARKET."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows13['id']=$row['ID'];
			$rows13['heading']=$row['heading'];
			$rows13['description']=$row['description'];
			$rows13['phno']=$row['contactNo'];
			$rows13['postDate']=$row['postDate'];			
			array_push($json_response13,$rows13);			
		}										
	}		
	
	$json_response14=array();
	$select1=mysql_query("select * from `".TABLE_RESOURCE."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows14['id']=$row['ID'];
			$rows14['name']=$row['name'];		
			$rows14['position']=$row['position'];	
			$rows14['phno']=$row['contactNo'];
			$rows14['type']=$row['type'];		
			array_push($json_response14,$rows14);			
		}										
	}
	
	$json_response15=array();
	$select1=mysql_query("select * from `".TABLE_SHOP_TYPE."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows15['id']=$row['ID'];
			$rows15['type']=$row['shopType'];				
			array_push($json_response15,$rows15);			
		}										
	}
		
	$json_response16=array();
	$select1=mysql_query("select * from `".TABLE_SHOP."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows16['id']=$row['ID'];
			$rows16['typeId']=$row['typeID'];	
			$rows16['name']=$row['name'];	
			$rows16['phno']=$row['phone'];			
			array_push($json_response16,$rows16);			
		}										
	}
	
	$json_response17=array();
	$select1=mysql_query("select * from `".TABLE_TAXI."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows17['id']=$row['ID'];
			$rows17['vehicleType']=$row['vehicleTypeId'];	
			$rows17['name']=$row['name'];	
			$rows17['phno']=$row['phone'];			
			array_push($json_response17,$rows17);			
		}										
	}
	
	$json_response18=array();
	$select1=mysql_query("select * from `".TABLE_TAXI_TYPE."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows18['id']=$row['ID'];
			$rows18['type']=$row['type'];				
			array_push($json_response18,$rows18);			
		}										
	}
	
	$json_response19=array();
	$select1=mysql_query("select * from `".TABLE_NOTIFICATION."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows19['id']=$row['ID'];
			$rows19['heading']=$row['heading'];	
			$rows19['description']=$row['description'];	
			$rows19['postDate']=$row['postDate'];			
			array_push($json_response19,$rows19);			
		}										
	}
	$json_response20=array();
	$select1=mysql_query("select * from `".TABLE_AD."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows20['id']=$row['ID'];	
			$rows20['last_date']=$row['endDate'];	
			$rows20['image_one_name']=$row['pic1'];			
			$rows20['image_two_name']=$row['pic2'];			
			array_push($json_response20,$rows20);			
		}										
	}
	
		$response = array();
        $response["BloodBankModel"] = $json_response1;
		$response["BusTimeOneModel"] = $json_response2;
		$response["BusTimeTwoModel"] = $json_response3;
		$response["CareerModel"] = $json_response4;
		$response["EmergancyModel"] = $json_response5;
		$response["HealthOneModel"] = $json_response6;
		$response["HealthTwoModel"] = $json_response7;
		$response["HealthThreeModel"] = $json_response8;
		$response["EducationOneModel"] = $json_response9;
		$response["EducationTwoModel"] = $json_response10;
		$response["JobOneModel"] = $json_response11;
		$response["JobTwoModel"] = $json_response12;
		$response["MarketModel"] = $json_response13;
		$response["ResourcePersionModel"] = $json_response14;
		$response["ShopOneModel"] = $json_response15;
		$response["ShopTwoModel"] = $json_response16;
		$response["AutoTaxiModel"] = $json_response17;
		$response["AutoTaxiModelOne"] = $json_response18;
		$response["NotificationModel"] = $json_response19;
		$response["AdSpaceModel"] = $json_response20;		
		
	    echo json_encode($response);
?>