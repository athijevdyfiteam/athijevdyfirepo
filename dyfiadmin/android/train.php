<?php
require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
// array for JSON response - 
	
	$json_train		=	array();
	$json_trainTime	=	array();
	
	$select1		=	mysql_query("select * from `".TABLE_TRAIN."`");		
	$number			=	mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows20['id']		=	$row['ID'];	
			$rows20['from']		=	$row['fromPlace'];	
			$rows20['to']		=	$row['toPlace'];			
			$rows20['days']		=	$row['days'];			
			$rows20['trainName']=	$row['trainName'];			
			$rows20['trainNum']	=	$row['trainNum'];			
			$rows20['via']		=	$row['via'];
						
			array_push($json_train,$rows20);			
		}										
	}
	$select2	=	mysql_query("select * from ".TABLE_TRAIN_TIME."");
	$number2	=	mysql_num_rows($select2);
	if($number2>0)
	{
		while($row2 = mysql_fetch_assoc($select2))
		{
			$rows21['id']		=	$row2['ID'];	
			$rows21['place']	=	$row2['place'];	
			$rows21['time']		=	$row2['time'];			
			$rows21['trainId']	=	$row2['trainId'];			

			array_push($json_trainTime,$rows21);			
		}
	}
	
	$response = array();
   	$response["Train"] 		= $json_train;
   	$response["TrainTime"] 	= $json_trainTime;
   	header('Content-type: application/json');
    echo json_encode($response);
?>