<?php
ini_set('memory_limit','-1');

require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

// array for JSON response - region
	$json_response2=array();	
	$select = mysql_query("select * from `".TABLE_REGION."` order by region ASC");	
	$number1=mysql_num_rows($select);
	if($number1>0)
	{		
		while($row1 = mysql_fetch_assoc($select))
		{
			$rows['id']=$row1['ID'];
			$rows['region']=ucfirst($row1['region']);
			
			array_push($json_response2,$rows);						
		}
	}

// array for JSON response - bloodbank
	$json_response1=array();	
	$select1=mysql_query("select * from `".TABLE_BLOOD."` ");		
	$number=mysql_num_rows($select1);
	if($number>0)
	{		
		while($row = mysql_fetch_assoc($select1))
		{
			$rows1['id']=$row['ID'];
			$rows1['bloodGroup']=$row['bloodGroup'];
			$rows1['name']=$row['name'];
			$rows1['phno']=$row['phone'];
			$rows1['regionId']=$row['regionId'];
			array_push($json_response1,$rows1);			
		}
	}
	
	$response = array();
    $response["Region"] = $json_response2;
    $response["BloodBankModel"] = $json_response1;
     echo json_encode($response);
?>