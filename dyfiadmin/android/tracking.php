<?php

require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

//history tracking

$url 	= 	file_get_contents('php://input');
$jsons	= 	urldecode($url);
if($jsons)
{
	$obj	=	json_decode($jsons);
	
	$date	=	$App->convert($obj->{'date'});

	$json_tracking 				= 	array();
	$json_region 				= 	array();
	$json_bloodbank 	 		= 	array();


	$qry	=	"select * from ".TABLE_TRACKING." where date>='".$App->dbformat_date_db($date)."' order by ID asc";
	$qryResult	=	mysql_query($qry);
	if(mysql_num_rows($qryResult)>0)
	{
		while($qryRow	=	mysql_fetch_array($qryResult))
		{
			
			$table 	= 	$qryRow['tableName'];
			$tid 	=	$qryRow['rowID'];
			
			$row['ID']			=	$qryRow['ID'];
			$row['tableName']	=	$qryRow['tableName'];
			$row['status']		=	$qryRow['status'];
			$row['rowID']		=	$qryRow['rowID'];
			$row['date']		=	$App->dbformat_date_db($qryRow['date']);
			
			array_push($json_tracking,$row); // first array from tracking tbl 
			
			if($table=='TABLE_REGION')
			{
				$qry1	=	mysql_query("select * from ".TABLE_REGION." where ID =$tid");
				if(mysql_num_rows($qry1)>0)
				{	
					while($res1	=	mysql_fetch_array($qry1))
					{
						$row1['id']			=	$res1['ID'];
						$row1['region']		=	$res1['region'];
						
						array_push($json_region,$row1);
					}				
				}
			}
			
			if($table=='TABLE_BLOOD')
			{
				$qry2	=	mysql_query("select * from ".TABLE_BLOOD." where ID =$tid");
				if(mysql_num_rows($qry2)>0)
				{	
					while($res2	=	mysql_fetch_array($qry2))
					{
						$row2['id']			=	$res2['ID'];
						$row2['bloodGroup']	=	$res2['bloodGroup'];
						$row2['name']		=	$res2['name'];
						$row2['phno']		=	$res2['phone'];
						$row2['regionId']	=	$res2['regionId'];
						
						array_push($json_bloodbank,$row2);
					}				
				}
			}
	
		}
	}
	
	$response	=	array();
	$response['Table Tracking']	=	$json_tracking;
	$response['Region']			=	$json_region;
	$response['BloodBankModel']	=	$json_bloodbank;
	echo json_encode($response);
}	   			   

?>