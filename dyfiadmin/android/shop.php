<?php
require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
// array for JSON response - shop type

	$json_response15=array();
	$select1=mysql_query("select * from `".TABLE_SHOP_TYPE."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows15['id']=$row['ID'];
			$rows15['type']=$row['shopType'];				
			array_push($json_response15,$rows15);			
		}										
	}
	// array for JSON response - shop

	$json_response16=array();
	$select1=mysql_query("select ".TABLE_SHOP.".ID,
								 ".TABLE_SHOP.".name,
								 ".TABLE_SHOP.".phone,
								 ".TABLE_SHOP.".typeID,
								 ".TABLE_REGION.".region
						   from `".TABLE_SHOP."`
					  left join	 ".TABLE_REGION." on ".TABLE_SHOP.".regionId=".TABLE_REGION.".ID
					  
						");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows16['id']		=	$row['ID'];
			$rows16['typeId']	=	$row['typeID'];	
			$rows16['name']		=	$row['name'];	
			$rows16['phno']		=	$row['phone'];			
			$rows16['region']	=	$row['region'];			
			array_push($json_response16,$rows16);			
		}										
	}
	
	$response = array();
   	$response["ShopOneModel"] = $json_response15;
   	$response["ShopTwoModel"] = $json_response16;
    echo json_encode($response);
?>