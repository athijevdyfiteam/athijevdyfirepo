<?php
require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

// array for JSON response - taxi type

	$json_response18=array();
	$select1=mysql_query("select * from `".TABLE_TAXI_TYPE."`");		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows18['id']=$row['ID'];
			$rows18['type']=$row['type'];				
			array_push($json_response18,$rows18);			
		}										
	}
// array for JSON response - taxi

	$json_response17=array();
	$selectQry	=	  "select ".TABLE_TAXI.".ID,
							  ".TABLE_TAXI_TYPE.".type,
							  ".TABLE_TAXI.".name,
							  ".TABLE_TAXI.".regNo,
							  ".TABLE_TAXI.".vehicleTypeId,
							  ".TABLE_TAXI.".phone,
							  ".TABLE_TAXI.".location
						 from ".TABLE_TAXI."
				   inner join ".TABLE_TAXI_TYPE."  on ".TABLE_TAXI_TYPE.".ID=".TABLE_TAXI.".vehicleTypeId
					 order by ".TABLE_TAXI.".ID desc";
	$select1=mysql_query($selectQry);		
	$number=mysql_num_rows($select1);	
	if($number>0)
	{	
		while($row = mysql_fetch_assoc($select1))
		{
			$rows17['id']			=	$row['ID'];
			$rows17['vehicleType']	=	$row['vehicleTypeId'];	
			$rows17['regNo']		=	$row['regNo'];	
			$rows17['name']			=	$row['name'];	
			$rows17['phno']			=	$row['phone'];			
			$rows17['location']		=	$row['location'];			
			array_push($json_response17,$rows17);			
		}										
	}
	
	
	$response = array();
	$response["AutoTaxiModelOne"] = $json_response18;
   	$response["AutoTaxiModel"] = $json_response17;
   	header('Content-type: application/json');
    echo json_encode($response);
?>