$(document).ready(function() {
	$('.leftmenu').prepend('<div class="mobmenu"></div>');
	$('.mobmenu').hide();
    mobileMenu();

    $(window).resize(mobileMenu);

    $('.datepicker').datepicker();
});

function mobileMenu(){
    if ($(window).width() <= 992){	
		 $('.mobmenu').show();
		 $('.leftmenu>ul').slideUp();
		 $('.mobmenu').click(function(){
		   $('.leftmenu>ul').stop().slideToggle();
		 });
	}
	else{
		$('.mobmenu').hide();
		$('.leftmenu>ul').slideDown();
	}	
}