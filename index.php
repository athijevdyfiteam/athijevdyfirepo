<?php
	require('config.inc.php');
	include('header.php');
?>

    <!-- Banner -->
    <div class="banner_wrap">
        <ul class="banner">
            <?php
            	$bannerSelect = 'select * from '.TABLE_BANNER. ' order by ID desc';
            	$bannerQry = mysqli_query($connection, $bannerSelect);
            	if(mysqli_num_rows($bannerQry) > 0){
					while($bannerRow = mysqli_fetch_array($bannerQry)){
						?>
						<li><img src="webadmin/<?= $bannerRow['bannerPath']; ?>" alt="" /></li>
						<?php
					}					
				}
            ?>
        </ul>
    </div>
    <!-- /Banner -->
    
    <!-- Welcome -->
    <div class="container wel_wrap">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="wel_img"><img src="images/welcome_img.png" alt="" /></div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="welcome_text">
                <h3>Welcome to</h3>
                <h2>Democratic  Youth Federation of India </h2>
                <div class="wel_kannur">
                    <h4>Kannur District Committee</h4>
                    <span></span>
                </div>
                <p>ഡി വൈ എഫ് ഐ (Democratic Youth Federation of India), 1980 നവംബർ 3നു പഞ്ചാബിലെ ലുധിയാനയിൽ രൂപം കൊണ്ടു. ഇന്ത്യയിലെ വിവിധ ഭാഗങ്ങളിലായി പ്രവർത്തിച്ചിരുന്ന യുവജന സംഘടനകളായ KSYF of Kerala, Socialist Valibar Organisation of Tamilnadu, Nav Jawan Sabha of Panjab, DYF of West Bengal തുടങ്ങിയ സംഘടനകളുടെ ഒരുമപ്പെടലാണ് ഡി വൈ എഫ് ഐ - യുടെ രൂപീകരണത്തിന് വഴിവെച്ചത്. നവംബർ 1 മുതൽ 3 വരെ ലുധിയാനയിൽ വെച്ച് നടന്ന ഈ സമ്മേളനത്തിലാണ് ഡി വൈ എഫ് ഐ - യുടെ പരിപാടിക്കും ഭരണഘടനക്കും രൂപം കൊടുത്തത്.</p>
                <div class="wel_lnk"><a href="aboutus.html">Read more</a></div>
            </div>
        </div>
    </div>
    <!-- /Welcome -->
    
    <!-- News -->
    <div class="news_wrap">
        <div class="news_inner">           
            <div class="container">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="hm_news_slider_wrap">
                        <h2>news and events</h2>
                        <ul class="hm_news_slider">
                        <?php
                        $eveSelect = "select * from ".TABLE_EVENTS." order by ID desc";
                        $eveQry = mysqli_query($connection, $eveSelect);
                        if(mysqli_num_rows($eveQry) > 0){
							while($eveRow = mysqli_fetch_array($eveQry)){
								$eventDate = $eveRow['eventDate'];
								list($y, $m, $d) = explode('-', $eventDate);
								$dateObj   = DateTime::createFromFormat('!m', $m);
								$monthName = $dateObj->format('F'); // March
							?>
							<li>
								<div class="hm_news_slider_inner">
                                    <div class="hm_news_head">
                                        <span class="hm_news_date"><span class="date"><?= $d ?></span><span class="month"><?= $monthName; ?></span></span>
                                        <h3><?= $eveRow['event']; ?></h3>
                                    </div>
                                    <p><?= $eveRow['description']; ?></p>
                                </div>
							</li>
							<?php
							}
						}
                        ?>
                        </ul>
                        <div class="hm_news_controls">
                            <span id="hm_news_prev"></span><span id="hm_news_next"></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="martyr_wrap">
                        <h3>Martyrs</h3>
                        <div class="martyr_inner">
                            <ul id="flexiselDemo2">
                                <li>
                                    <img src="images/martyr.jpg" alt="" />
                                    <p>രാജീവൻ</p>
                                </li>
                                <li>
                                    <img src="images/martyr2.jpg" alt="" />
                                    <p>ബാബു</p>
                                </li>
                                <li>
                                    <img src="images/martyr3.jpg" alt="" />
                                    <p>റോഷൻ</p>
                                </li>
                                <li>
                                    <img src="images/martyr4.jpg" alt="" />
                                    <p>മധു</p>
                                </li>
                                <li>
                                    <img src="images/martyr5.jpg" alt="" />
                                    <p>ഷിബുലാൽ </p>
                                </li>
                            </ul>
                        </div>
                        <div class="bd_clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- News -->
    
    <!-- Gallery -->
    <div class="container">
        <div class="gallery_head">
            <h3>gallery</h3>
        </div>
        <div class="gal_slider_wrap">
            <ul id="flexiselDemo3">
            <?php
            	$hmGalSelect = "select * from ".TABLE_GALLERY." order by ID desc";
            	$hmGalQry = mysqli_query($connection, $hmGalSelect);
            	if(mysqli_num_rows($hmGalQry) > 0){
					while($hmGalRow = mysqli_fetch_array($hmGalQry)){
					?>
						<li><img src="webadmin/<?= $hmGalRow['galleryPath'] ?>" alt="" /></li>
					<?php
					}
				}
            ?>
            </ul>
            <div class="bd_clear"></div>
        </div>
    </div>
    <!-- /Gallery -->

<?php  include('footer.php'); ?>